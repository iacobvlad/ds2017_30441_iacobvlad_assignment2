import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import Entities.Car;
import Services.ISellingPrice;

public class SellingPrice extends UnicastRemoteObject implements ISellingPrice {

	protected SellingPrice() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public double computePrice(Car c) {
		// TODO Auto-generated method stub

		double sellingPrice = 0 ;
		if (c.getYear() < 0) {
			throw new IllegalArgumentException("Year must be positive.");
		}
		sellingPrice = c.getPurchasingPrice() - (c.getPurchasingPrice() / 7 ) * (2015 - c.getYear());

		return sellingPrice;
	}

}
