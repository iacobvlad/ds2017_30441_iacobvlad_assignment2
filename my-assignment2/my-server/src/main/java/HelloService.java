import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import Services.IHelloService;

public class HelloService extends UnicastRemoteObject implements IHelloService{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected HelloService() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	public String echo(String message) throws RemoteException {
		// TODO Auto-generated method stub
		return "From server" + message;
	}

}
