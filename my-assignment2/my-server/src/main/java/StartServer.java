import java.rmi.AccessException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class StartServer {
	public static void main(String[] args) throws AccessException, RemoteException {
		Registry registry = LocateRegistry.createRegistry(3196);
		registry.rebind("hello", new HelloService());
		registry.rebind("computeTax", new TaxService());
		registry.rebind("computePrice", new SellingPrice());
	}
}
