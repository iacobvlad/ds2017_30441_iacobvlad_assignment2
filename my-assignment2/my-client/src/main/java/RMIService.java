import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import Entities.Car;
import Services.IHelloService;
import Services.ISellingPrice;
import Services.ITaxService;

public class RMIService {

	private ITaxService taxService;
	private ISellingPrice sellPrice;
	
	public RMIService() {
		
		 try {
			taxService = (ITaxService) Naming.lookup("rmi://localhost:3196/computeTax");
			sellPrice = (ISellingPrice) Naming.lookup("rmi://localhost:3196/computePrice");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String computeTax(Car c ) throws RemoteException {
		return String.valueOf(taxService.computeTax(c));
	}
	
	public String computePrice(Car c) throws RemoteException {
		NumberFormat formatter = new DecimalFormat("#0.000�");    
		String price = formatter.format(sellPrice.computePrice(c));
		return price;
	}
}
