import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.rmi.server.Operation;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ClientUI extends JFrame {
	
	public JTextField cubicCapacity, purchasePrice, taxPrice, sellingPrice;
	public JButton compute;
	public JPanel panel;
	
	public ClientUI() {
		
		panel = new JPanel(new FlowLayout());
		compute = new JButton("Compute");
		this.setSize(new Dimension(500, 100));
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		cubicCapacity = new JTextField("Cubic capcity...");
		purchasePrice = new JTextField("Purchase price...");
		taxPrice = new JTextField("Tax price              ");
		sellingPrice = new JTextField("Sell price         ");
		purchasePrice.setBackground(Color.white);
		cubicCapacity.setBackground(Color.white);
		taxPrice.setBackground(Color.lightGray);
		sellingPrice.setBackground(Color.lightGray);
		taxPrice.setEditable(false);
		sellingPrice.setEditable(false);
	
		panel.add(cubicCapacity);
		panel.add(purchasePrice);
		panel.add(compute);
		panel.add(taxPrice);
		panel.add(sellingPrice);
		this.add(panel);
		this.setVisible(true);
		
	}
	
	public void setComputeActionListener(ActionListener action) {
		this.compute.addActionListener(action);
	}
	
	public String getCubicCapacityText() {
		return cubicCapacity.getText();
	}

	public double getPrice() {
		// TODO Auto-generated method stub
		System.out.println(purchasePrice.getText());
		System.out.println(Double.valueOf(purchasePrice.getText()));
		return Double.valueOf(purchasePrice.getText());
	}

	public int getCC() {
		// TODO Auto-generated method stub
		System.out.println(cubicCapacity.getText());
		System.out.println(Integer.valueOf(cubicCapacity.getText()));
		return Integer.valueOf(cubicCapacity.getText());
	}
	
	public void setTax(String tax) {
		taxPrice.setText(tax+ "�");
	}
	
	public void setPrice(String price) { 
		sellingPrice.setText(price);
	}
}
