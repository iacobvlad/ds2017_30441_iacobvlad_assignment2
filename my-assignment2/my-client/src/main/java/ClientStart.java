import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JFrame;

import Entities.Car;
import Services.IHelloService;
import Services.ISellingPrice;
import Services.ITaxService;

public class ClientStart {
	 
	public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
		RMIService rmi = new RMIService();
		ClientUI ui = new ClientUI();
		ui.setComputeActionListener(new ComputePrices(ui,rmi));	
	}
}

class ComputePrices implements ActionListener{
	
	private ClientUI originFrame;
	private RMIService rmi;
	
	public ComputePrices(ClientUI originFrame,RMIService rmi) {
		this.originFrame = originFrame;
		this.rmi = rmi;
	}
	
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		Car c = new Car(2010, originFrame.getCC(), originFrame.getPrice());
		System.out.println(c.toString());
		try {
			originFrame.setTax(rmi.computeTax(c));
			originFrame.setPrice(rmi.computePrice(c));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}