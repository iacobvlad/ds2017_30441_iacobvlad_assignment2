package Services;

import java.rmi.Remote;
import java.rmi.RemoteException;

import Entities.Car;

public interface ITaxService extends Remote {

	double computeTax(Car c) throws RemoteException;
}
