package Services;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IHelloService extends Remote{
	
	public String echo(String message) throws RemoteException;
}
