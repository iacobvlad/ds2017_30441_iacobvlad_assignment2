package Services;

import java.rmi.Remote;
import java.rmi.RemoteException;

import Entities.Car;

public interface ISellingPrice extends Remote{
	
	double computePrice(Car c) throws RemoteException;
}
